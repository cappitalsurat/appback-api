## Getting started
1. Build your library
  * Run `npm install` to get the project's dependencies
  * Run `npm run build` to produce minified version of your library.
2. Development mode
  * Having all the dependencies installed run `npm run dev`. This command will generate an non-minified version of your library and will run a watcher so you get the compilation on file change.
3. Running the tests
  * Run `npm run test`

## Intended Use (WIP)

**Note**: Use *fetch* for get APIs


```js

const appbackApi = new AppBackApi(); `Done`
appbackApi.login().then() `Done`
appbackApi.logout().then() `Done`

const todoRest = appbackApi.getModelRest('Todo'); `Done`
todoRest.create({}).then() `Done`
todoRest.all({}).fetch().then() `Done`

todoRest.one(id).fetch().then()
todoRest.one(id).one('Attachment', id).fetch().then()
todoRest.one(id).all('Attachment',{}).fetch().then()

var todo = todoRest.one(id); `Done`
todo.delete().then() `Done`
todo.update(data).then() `Done`
```


## Scripts

* `npm run build` - produces production version of your library under the `lib` folder
* `npm run dev` - produces development version of your library and runs a watcher
* `npm run test` - well ... it runs the tests :)

## Misc

### An example of using dependencies that shouldn’t be resolved by webpack, but should become dependencies of the resulting bundle

In the following example we are excluding React and Lodash:

```js
{
  devtool: 'source-map',
  output: {
    path: '...',
    libraryTarget: 'umd',
    library: '...'
  },
  entry: '...',
  ...
  externals: {
    react: 'react'
    // Use more complicated mapping for lodash.
    // We need to access it differently depending
    // on the environment.
    lodash: {
      commonjs: 'lodash',
      commonjs2: 'lodash',
      amd: '_',
      root: '_'
    }
  }
}
```

# Origin
Created from https://github.com/krasimir/webpack-library-starter
Webpack based boilerplate for producing libraries (Input: ES6, Output: universal library)

## Readings
* [Start your own JavaScript library using webpack and ES6](http://krasimirtsonev.com/blog/article/javascript-library-starter-using-webpack-es6)