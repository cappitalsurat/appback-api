import chai from 'chai';
import AppbackApi from '../lib/appback-api.js';

chai.expect();

const expect = chai.expect;

let lib;
let abModelInstance;

describe('Instance of AppbackApi', function () {
  it('should throw error', () => {
    var createWrapper = function () { new AppbackApi() };
    expect(createWrapper).to.throw(Error);
  });

  it('should return the name', () => {
    lib = new AppbackApi('http://localhost:4000/api');
    expect(lib.name).to.be.equal('AppbackApi');
  });
});

describe('should login', function () {
  it('should login', (next) => {
    //expect().to.not.be.instanceof(Error);
    lib.login({
      'email': 'admin@appback.com',
      'password': 'admin@appback'
    }).then((data) => {
      // const models = data.body();
      console.log("success DATA", data);
      next();
    }, (error) => {
      console.log("error DATA", error.toString());
      next(error);
    }).catch(function (error) {
      console.log("catch DATA", error);
      next(error);
    });
  });
});

describe('isLoggedIn', function () {
  it('isLoggedIn', () => {
    //expect().to.not.be.instanceof(Error);
    console.log('isLoggedIn', lib.isLoggedIn());
  });
});

describe('Create a Model (AbModel) Instance for rest', function () {
  let modelId;
  it('Should create refernce to call APIs', () => {
    abModelInstance = lib.createModelRest('AbModels', null);
    expect(abModelInstance).to.not.be.a('null')
      .and.not.be.an('undefined');
  });

  describe('All Method', function () {
    it('Should return array of Abmodels', (next) => {
      abModelInstance.all().fetch().then((response) => {
        // console.log("RES > ", response.error);
        expect(response).to.be.an('array');
        next();
      }, (error) => {
        console.log("ERROR > ", error);
        next(error);
      })
    });
  });
  describe('All Method', function () {
    it('Should return array of Abmodels', (next) => {
      abModelInstance.all().fetch().then((response) => {
        expect(response).to.be.an('array');
        next();
      }, (error) => {
        next(error);
      })
    });

    it('Get models in AbModels with filter (Model name: TodoList )', (next) => {
      let filter = {
        where: {
          name: 'TodoList'
        }
      }
      abModelInstance.all(filter).fetch().then((response) => {
        const models = response;

        const Model = models[0];
        //console.log(Model.name);
        expect(Model.name).to.be.equal('TodoList');
        next();
      }, (error) => {
        next(error);
      });
    });
  });

  describe('Create Model Method', function () {
    it('Should create entry for abmodel', (next) => {
      let data = {
        "name": new Date().toString(),
        "description": "description",
        "type": "PERSISTED_MODEL",
        "access": [],
        "validateType": "TRUE",
        "relations": []
      }
      abModelInstance.create(data).then((resp) => {
        modelId = resp.id;
        next();
      }, (error) => {
        next(error);
      });
    });
  });

  describe('Count Model Method', function () {
    it('Should create count for abmodel', (next) => {
      abModelInstance.count().fetch().then((resp) => {
        try {
          expect(resp.count).to.be.a('Number');
          next();
        } catch (exec) {
          next(exec);
        }
      }, (error) => {
        next(error);
      });
    });

    it('Should create count for abmodel with filter (name: TodoList)', (next) => {
      const where = {
        name: 'TodoList'
      }
      abModelInstance.count(where).fetch().then((resp) => {
        try {
          expect(resp.count).to.be.equal(1);
          next();
        } catch (exec) {
          next(exec);
        }
      }, (error) => {
        next(error);
      });
    });
  });

  describe('One Method', function () {
    it('Should return object of Abmodel', (next) => {
      abModelInstance.one(modelId).fetch().then((response) => {
        expect(response).to.be.an('object');
        next();
      }, (error) => {
        next(error);
      })
    });

    it('Get model in AbModel with filter (fileds: [name, type] )', (next) => {
      let filter = {
        fields: ['name', 'type']
      }
      abModelInstance.one(modelId, filter).fetch().then((response) => {
        let model = response;
        expect(model).to.have.all.keys('name', 'type');
        next();
      }, (error) => {
        next(error);
      });
    });
  });

  describe('Update Model Method', function () {
    it('Update model', (next) => {
      const name = 'Updated Name'
      let updateData = {
        name
      }
      abModelInstance.one(modelId).update(updateData).then((response) => {
        let model = response;
        expect(model.name).to.be.equal(name);
        next();
      }, (error) => {
        next(error);
      });
    });
  });

  describe('FindOne Model Method', function () {
    it('Find One model', (next) => {
      let filter = {
        where: {
          name: 'TodoList'
        }
      }
      abModelInstance.findOne(filter).fetch().then((response) => {
        const model = response;
        //console.log(Model.name);
        expect(model.name).to.be.equal('TodoList');
        next();
      }, (error) => {
        next(error);
      });
    });
  });

  describe('Delete Model Method', function () {
    it('Delete model', (next) => {
      abModelInstance.one(modelId).delete().then((response) => {
        next();
      }, (error) => {
        next(error);
      });
    });
  });
});

describe('Test Create Method', function () {
  let modelInstance;
  before(function () {
    modelInstance = lib.createModelRest('AbModels', null);
  });

  describe('Create Method with expected data', function () {
    it('Should create entry for abmodel', (next) => {
      let data = {
        "name": new Date().toString(),
        "description": "description",
        "type": "PERSISTED_MODEL",
        "access": [],
        "validateType": "TRUE",
        "relations": []
      }
      modelInstance.create(data).then((resp) => {
        try {
          expect(resp).to.be.an('Object');
          next();
        } catch (exec) {
          next(exec);
        }
      }, (error) => {
        next(error);
      });
    });
  });

  describe('Create Method without required data', function () {
    it('Should throw error', (next) => {
      let data = {};
      modelInstance.create(data).then((resp) => {
        next(resp);
      }, (error) => {
        expect(error).to.be.an('Object');
        next();
      });
    });
  });
});

describe('Test Delete Method', function () {
  let modelInstance;
  let modelId;
  before(function (next) {
    modelInstance = lib.createModelRest('AbModels', null);
    let data = {
      "name": new Date().toString(),
      "description": "description",
      "type": "PERSISTED_MODEL",
      "access": [],
      "validateType": "TRUE",
      "relations": []
    }
    modelInstance.create(data).then((resp) => {
      modelId = resp.id;
      next();
    }, (error) => {
      next(error);
    });
  });

  describe('Delete Model', function () {
    it('should return count 1', (next) => {
      modelInstance.one(modelId).delete().then((response) => {
        expect(response.count).to.be.equal(1);
        next();
      }, (error) => {
        next(error);
      });
    });
  });

  describe('Delete Aready Deleted Model', function () {
    it('should return count 0', (next) => {
      modelInstance.one(modelId).delete().then((response) => {
        expect(response.count).to.be.equal(0);
        next();
      }, (error) => {
        next(error);
      });
    });
  });
});

describe('Test Update Method', function () {
  let modelInstance;
  let modelId;
  before(function (next) {
    modelInstance = lib.createModelRest('AbModels', null);
    let data = {
      "name": new Date().toString(),
      "description": "description",
      "type": "PERSISTED_MODEL",
      "access": [],
      "validateType": "TRUE",
      "relations": []
    }
    modelInstance.create(data).then((resp) => {
      modelId = resp.id;
      next();
    }, (error) => {
      next(error);
    });
  });

  describe('Update Model', function () {
    it('Update model', (next) => {
      let updateData = {
        name: 'updated name'
      }
      modelInstance.one(modelId).update(updateData).then((response) => {
        try {
          expect(response.name).to.be.equal(updateData.name);
          next();
        } catch (exec) {
          next(exec);
        }
      }, (error) => {
        next(error);
      });
    });
  });

  describe('Update Model with property not defined in model', function () {
    it('should throw error', (next) => {
      let updateData = {
        desc: 'desc not present',
        name: 'new updated name'
      }
      modelInstance.one(modelId).update(updateData).then((response) => {
        // console.log("resp", response.body().data());
        next(response);
      }, (error) => {
        expect(error).to.be.instanceof(Object);
        next();
      });
    });
  });
});

// describe('custom method', function () {
//   let modelInstance;
//   let modelId;
//   before(function () {
//     modelInstance = lib.createModelRest('AbModels', null);
//   });
//   it('should return string', (next) => {
//     //expect().to.not.be.instanceof(Error);
//     modelInstance.custom('greet', {
//       msg: 'msg1',
//       data: 'data1'
//     }, {
//       data: 'data2',
//       msg: 'msg2'
//     }, {
//       type: 'POST'
//     }).then((data) => {
//       console.log("DATA ", data.body().data());
//       next();
//     }, (error) => {
//       console.log("DATA", error.toString());
//       next();
//     });
//   });

//   it('should return string', (next) => {
//     //expect().to.not.be.instanceof(Error);
//     modelInstance.custom('greetGet', {
//       //msg: 'abcdef'
//     }, {
//       msg: 'abcde'
//     }, {}).then((data) => {
//       console.log("DATA ", data.body().data());
//       next();
//     }, (error) => {
//       console.log("DATA", error.toString());
//       next();
//     });
//   });
// });

describe('should logout', function () {
  it('should logout', (next) => {
    //expect().to.not.be.instanceof(Error);
    lib.logout().then((data) => {
      next();
    }, (error) => {
      console.log("Error", error.toString());
      next();
    });
  });
});

// describe('signed url', function () {
//   it('get url', (next) => {
//     //expect().to.not.be.instanceof(Error);
//     lib.getUploadUrl({
//       extension: 'png',
//       contentType: 'images/png'
//     }).then((data) => {
//       console.log('abcd');
//       console.log("data", data);
//       next();
//     }, (error) => {
//       console.log("Error", error.toString());
//       next();
//     });
//   });
// });
