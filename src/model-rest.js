const _ = require('lodash');

class ModelRest {
  constructor(collection, api, options) {
    this.collection = collection;
    this.api = api;
  }

  all(filter, options = { headers: {} }) {
    var apiCollection = this.api.all(this.collection);
    return {
      fetch: () => {
        return restCall(apiCollection.getAll({
          filter: JSON.stringify(filter)
        }, options.headers));
      }
    }
  }

  one(id, filter, options = { headers: {} }) {
    var apiModel = this.api.one(this.collection, id);
    return {
      fetch: () => {
        return restCall(apiModel.get({ filter: JSON.stringify(filter) }, options.headers));
      },
      one: () => {

      },
      all: () => {

      },
      summary: () => {
        //TODO: Return child count
      },
      delete: (deleteDeps) => {
        //TODO: Pass Delete Dependencies to server & decide whether to delete deps or not
        return restCall(apiModel.delete(null, null, options.headers));
      },
      update: (data) => {
        return restCall(apiModel.patch(data, null, options.headers));
      }
    }
  }

  create(data, options = { headers: {} }) {
    return restCall(this.api.all(this.collection).post(data, null, options.headers));
  }

  count(where, options = { headers: {} }) {
    var countModel = this.api.custom(`${this.collection}/count`);
    return {
      fetch: () => {
        return restCall(countModel.get({ where: JSON.stringify(where) }, options.headers));
      }
    }
  }

  custom(name, data, params, options = { type: 'GET', headers: {} }) {
    data = data || {};
    params = params || {};
    if (params.filter) {
      params.filter = JSON.stringify(params.filter);
    }
    if (options && options.type === 'POST') {
      return restCall(this.api.custom(`${this.collection}/${name}`).post(data, params, options.headers));
    } else {
      return restCall(this.api.custom(`${this.collection}/${name}`).get(params, options.headers));
    }
  }

  findOne(filter, options = { headers: {} }) {
    var findOneModel = this.api.custom(`${this.collection}/findOne`);
    return {
      fetch: () => {
        return restCall(findOneModel.get({ filter: JSON.stringify(filter) }, options.headers));
      }
    }
  }
}

const restCall = (rest) => {
  return rest.then(function (response) {
    const body = response.body() ? response.body() : {};
    if (Array.isArray(body)) {
      let data = [];
      body.forEach(function (element) {
        data.push(element.data());
      });
      return data;
    } else {
      return body.data() ? body.data() : {};
    }
  }, function (error) {
    // console.log('Error > ', error.response.data.error);
    const errResp = error.response.data.error;
    throw errResp;
  });
}

export default ModelRest; 