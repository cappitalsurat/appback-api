import 'whatwg-fetch';
import restful, { fetchBackend } from 'restful.js';
import ModelRest from './model-rest';
import validate from 'validate-params-schema';
import * as io from 'socket.io-client';

const disconnectSocket = (socket) => {
  if (socket) {
    console.log('SOCKET DISCONNECTED');
    socket.disconnect();
  }
}

const connectSocket = (options, token, userId) => {
  if (options.socketConnection) {
    if (!options.host) {
      throw Error('Host url is required for socket connection');
    }
    let socket = io.connect(options.host);
    socket.on('connect', () => {
      console.log('SOCKET CONNECTED');

      //AUTHENTICATE USER
      socket.emit('authentication', { id: token, userId: userId });

      socket.on('authenticated', function () {
        console.log('User is authenticated');
      });

      socket.on('unauthorized', () => {
        console.log('User is not authenticated');
        localStorage.clear();
        disconnectSocket(socket);
      });
    });
    return socket;
  }
}

export default class Library {
  constructor(apiUrl, options) {
    console.log('creating instance with : ' + apiUrl)
    options = options || {};
    if (!apiUrl || !apiUrl instanceof String) {
      throw Error('apiUrl (String) required');
    }
    this._name = 'AppbackApi';
    this._init(apiUrl, options);
  }

  get name() {
    return this._name;
  }

  getToken() {
    if (this.isMobileApp) {
      return this.options.getToken();
    } else {
      return localStorage.getItem(this.apiUrl + '_Token');
    }
  }
  getUser() {
    if (this.isMobileApp) {
      return this.options.getUser();
    } else {
      return localStorage.getItem(this.apiUrl + '_userId');
    }
  }
  _init(_apiUrl, _options) {
    // console.log('init', this.apiUrl);
    if (this.apiUrl) {
      throw Error('already initialized');
    }
    this.apiUrl = _apiUrl;
    this.options = _options;
    this.isMobileApp = this.options && this.options.getToken;

    this.api = restful(this.apiUrl, fetchBackend(fetch));
    var token = this.getToken();
    var userId = this.getUser();

    if (token) {
      this.api.header('Authorization', token);
      if (!this.isMobileApp) {
        this.socket = connectSocket(this.options, token, userId);
      }
    }

    this.api.on('error', (error, config) => {
      // console.log('ERROR > ', error.response.data.error);
      //&& (error.response.data.error.message == 'LOGIN_REQUIRED')
      if (error.response.data && error.response.data.error && (error.response.data.error.statusCode == 405)) {
        this.api.header('Authorization', undefined)
        if (!this.isMobileApp) {
          localStorage.clear();
          disconnectSocket(this.socket);
        }

      }
    });
  }

  login(object, include, modelName, options = { headers: {} }) {
    //var validate = require('validate-params-schema');
    var model = modelName || 'abusers';
    var restUrl = `${model}/login`;
    console.log('REST URL > ', modelName, model, restUrl);
    var schema = {
      'email': { required: true, type: String },
      'password': { required: true, type: String },
      'ttl': { required: false, type: Number }
    }
    var includeFilter = JSON.stringify(include) || {};
    var errors = validate(object, schema)
    if (errors) {
      throw Error(errors.toString());
    }
    else {
      return this.api.custom(restUrl).post(object, { include: includeFilter }, options.headers).then((response) => {
        // console.log('response', response)
        let token = response.body().data().id;
        let userId = response.body().data().userId;
        this.api.header('Authorization', token);
        if (!this.isMobileApp) {
          localStorage.setItem(this.apiUrl + '_Token', token);
          localStorage.setItem(this.apiUrl + '_userId', userId);
          this.socket = connectSocket(this.options, token, userId);
        }
        return response.body().data();
      }, (error) => {
        // console.log('error', error.response.data.error)
        const errResp = error.response.data.error;

        throw errResp;
      });
      // return this.api.('abusers/login', object);
    }
  }

  isLoggedIn() {
    // let token = localStorage.getItem(this.apiUrl + '_Token');
    let token = this.getToken();
    if (token) {
      return true;
    } else {
      return false;
    }
  }

  logout(modelName, options = { headers: {} }) {
    //console.log('logout called')
    var model = modelName || 'abusers';
    var restUrl = `${model}/logout`;
    return this.api.custom(restUrl).post(null, null, options.headers).then((response) => {
      this.api.header('Authorization', undefined)
      if (!this.isMobileApp) {
        localStorage.clear();
        disconnectSocket(this.socket);
      }
      return response;
    }, (error) => {
      // console.log('error', error.response.data.error)
      const errResp = error.response.data.error;
      throw errResp;
    });
  }

  changePassword(data, modelName) {
    var model = modelName || 'abusers';
    var restUrl = `${model}/changePassword`;

    return this.api.custom(restUrl).post(data).then((response) => {
      return response.body().data();
    }, (error) => {
      const errResp = error.response.data.error;
      throw errResp;
    });
  }

  createModelRest(model, options) {
    // console.log('createModelRest', this);
    if (!this.apiUrl) {
      throw Error('init not called yet with required params.')
    }
    return new ModelRest(model, this.api, options);
  }

  getUploadUrl(data) {
    let url = 'service/getSignedUrl';
    return this.api.custom(url).post(data).then((response) => {
      // console.log('RESP > ', response.body().data());
      return response.body().data();
    }, (error) => {
      const errResp = error.response.data.error;
      throw errResp;
    });
  }

  getRestfulApi() {
    return this.api;
  }

  _connectSocket(token, userId) {
    if (this.options.socketConnection) {
      if (!this.options.host) {
        throw Error('Host url is required for socket connection');
      }
      let _socket = io.connect(this.options.host);
      _socket.on('connect', () => {
        console.log('SOCKET CONNECTED');

        //AUTHENTICATE USER
        _socket.emit('authentication', { id: token, userId: userId });

        _socket.on('authenticated', function () {
          console.log('User is authenticated');
        });

        _socket.on('unauthorized', () => {
          console.log('User is not authenticated');
          localStorage.clear();
          disconnectSocket(_socket);
        });
      });
      this.socket = _socket;
    }
  }

  _disconnectSocket() {
    disconnectSocket(this.socket);
  }
}
