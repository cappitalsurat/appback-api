var webpack = require('karma-webpack');
var webpackConfig = require('./webpack.config');

//webpackConfig.entry = {};
console.log('using config ', webpackConfig)
module.exports = function (config) {
    config.set({
        frameworks: ['mocha'],
        files: [
            'node_modules/babel-polyfill/dist/polyfill.js',
            './src/index.js',
            //'./node_modules/angular-mocks/angular-mocks.js',
            './test/**/*.js'
        ],
        plugins: [
            webpack,
            'karma-mocha',
            'karma-chrome-launcher',
            //'karma-firefox-launcher',
            'karma-phantomjs-launcher',
            'karma-coverage',
            'karma-spec-reporter'
        ],
        browsers: ['PhantomJS'],
        preprocessors: {
            './test/**/*.spec.js': ['webpack'],
            './src/index.js': ['webpack'],
        },
        reporters: ['spec', 'coverage'],
        coverageReporter: {
            dir: 'build/reports/coverage',
            reporters: [
                { type: 'html', subdir: 'report-html' },
                { type: 'lcov', subdir: 'report-lcov' },
                { type: 'cobertura', subdir: '.', file: 'cobertura.txt' }
            ]
        },
        // webpack: webpackConfig,
        webpack: {
            module: {
                loaders: [
                    {
                        test: /\.js$/,
                        exclude: /node_modules/,
                        loader: 'babel'
                    }
                ]
            },
            externals: {
                cheerio: 'window',
                'react/addons': true,
                'react/lib/ExecutionEnvironment': true,
                'react/lib/ReactContext': true
            },
            node: {
                fs: 'empty'
            }
        },
        webpackMiddleware: { noInfo: true }
    });
};